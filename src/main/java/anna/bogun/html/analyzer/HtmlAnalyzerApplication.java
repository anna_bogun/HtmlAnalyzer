package anna.bogun.html.analyzer;

import anna.bogun.html.analyzer.exception.SimilarElementNotFoundException;
import anna.bogun.html.analyzer.model.ElementInfo;
import anna.bogun.html.analyzer.service.DocumentParser;
import anna.bogun.html.analyzer.service.ElementFinder;
import anna.bogun.html.analyzer.service.SimilarityIndexCalculator;

public class HtmlAnalyzerApplication {

    public static void main(String[] args) throws SimilarElementNotFoundException {
        String targetElementId = args[0];
        String inputOriginPage = args[1];
        String anotherPage = args[2];

        ElementFinder elementFinder = new ElementFinder(new DocumentParser(new SimilarityIndexCalculator()));
        String result = elementFinder.findSimilarElementCssSelector(
                new ElementInfo(inputOriginPage, targetElementId),
                new ElementInfo(anotherPage)
        );

        System.out.println(result);
    }
}
