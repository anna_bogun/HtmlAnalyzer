package anna.bogun.html.analyzer.model;

import org.jsoup.nodes.Element;

import java.io.File;

public class ElementInfo {
    private String elementId;
    private File resourceFile;
    private Element element;

    public ElementInfo(String resourcePath, String elementId) {
        this.resourceFile = new File (resourcePath);
        this.elementId = elementId;
    }

    public ElementInfo(String resourcePath) {
        this.resourceFile = new File(resourcePath);
    }

    public File getResourceFile() {
        return resourceFile;
    }

    public void setResourceFile(File resourceFile) {
        this.resourceFile = resourceFile;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }
}
