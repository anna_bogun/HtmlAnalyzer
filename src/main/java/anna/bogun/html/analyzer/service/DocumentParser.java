package anna.bogun.html.analyzer.service;

import anna.bogun.html.analyzer.model.ElementInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class DocumentParser {
    private static Logger LOG = LogManager.getLogger(DocumentParser.class);
    private static String CHARSET_NAME = "utf8";

    private SimilarityIndexCalculator similarityIndexCalculator;

    public DocumentParser(SimilarityIndexCalculator similarityIndexCalculator) {
        this.similarityIndexCalculator = similarityIndexCalculator;
    }

    public Optional<Element> findElementById(ElementInfo elementInfo) {
        File file = elementInfo.getResourceFile();
        try {
            return Optional.of(parseDocument(file).getElementById(elementInfo.getElementId()));
        } catch (IOException e) {
            LOG.error("Error reading [{}] file", file.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    public Optional<Element> findSimilarElement(ElementInfo original, ElementInfo another) {
        File file = another.getResourceFile();
        try {
            return findSimilarElement(original, another, parseDocument(file));
        } catch (IOException e) {
            LOG.error("Error reading [{}] file", file.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    private Optional<Element> findSimilarElement(ElementInfo original, ElementInfo another, Document document) {
        return original.getElement().attributes()
                .asList()
                .stream()
                .flatMap(attribute -> document
                                .getElementsByAttributeValue(attribute.getKey(), attribute.getValue())
                                .stream())
                .max(similarityIndexCalculator.getComparator(original.getElement()));
    }

    private Document parseDocument(File file) throws IOException {
        return Jsoup.parse(file, CHARSET_NAME, file.getAbsolutePath());
    }
}
