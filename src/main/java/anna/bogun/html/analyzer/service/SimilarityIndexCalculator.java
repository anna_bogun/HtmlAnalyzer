package anna.bogun.html.analyzer.service;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;

import java.util.Comparator;
import java.util.Objects;

public class SimilarityIndexCalculator {

    public int calculateSimilarityIndex(Element tested, Element original) {
        int similarityIndex = 0;
        for (Attribute attribute : tested.attributes().asList()) {
            String originalValue = original.attr(attribute.getKey());
            if (Objects.isNull(originalValue)) {
                continue;
            }
            if (attribute.getValue().equals(originalValue)) {
                similarityIndex++;
            }
        }
        return similarityIndex;
    }

    public Comparator<Element> getComparator(Element origin) {
        return Comparator.comparingInt(element -> calculateSimilarityIndex(element, origin));
    }
}
