package anna.bogun.html.analyzer.service;

import anna.bogun.html.analyzer.exception.SimilarElementNotFoundException;
import anna.bogun.html.analyzer.model.ElementInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Element;

import java.util.Optional;


public class ElementFinder {

    private static Logger LOG = LogManager.getLogger(ElementFinder.class);
    private DocumentParser documentParser;

    public ElementFinder(DocumentParser documentParser) {
        this.documentParser = documentParser;
    }

    public ElementInfo findElementById(ElementInfo elementInfo) {
        elementInfo.setElement(documentParser.findElementById(elementInfo).get());
        return elementInfo;
    }

    public Optional<Element> findSimilarElement(ElementInfo original, ElementInfo another) {
        Optional<Element> element = documentParser.findSimilarElement(findElementById(original), another);
        LOG.info("New similar element [{}] was found", element.get());
        return element;
    }

    public String findSimilarElementCssSelector(ElementInfo original, ElementInfo another)
            throws SimilarElementNotFoundException {
        String cssSelector = findSimilarElement(original, another)
                .orElseThrow(() -> new SimilarElementNotFoundException())
                .cssSelector();
        LOG.info("New css selector [{}] was found", cssSelector);
        return cssSelector;
    }
}
