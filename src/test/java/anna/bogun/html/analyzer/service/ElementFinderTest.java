package anna.bogun.html.analyzer.service;

import anna.bogun.html.analyzer.exception.SimilarElementNotFoundException;
import anna.bogun.html.analyzer.model.ElementInfo;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ElementFinderTest {

    private ElementInfo original;
    private ElementInfo another;
    private String elementId;
    private String originPagePath;
    private String anotherPagePath;
    private ElementFinder elementFinder;

    @Before
    public void setUp() {
        elementId = "make-everything-ok-button";
        originPagePath = this.getClass().getResource("/test_data/sample-0-origin.html").getPath();
        anotherPagePath = this.getClass().getResource("/test_data/sample-1-evil-gemini.html").getPath();
        original = new ElementInfo(originPagePath, elementId);
        another = new ElementInfo(anotherPagePath, elementId);
        elementFinder = new ElementFinder(new DocumentParser(new SimilarityIndexCalculator()));
    }

    @Test
    public void findElementById() {
        ElementInfo elementInfo = elementFinder.findElementById(original);
        assertNotNull(elementInfo.getElement());
        assertEquals(elementInfo.getElement().attr("id"), elementId);
    }

    @Test
    public void findSimilarElement() {
        Element element = elementFinder.findSimilarElement(original, another).get();
        assertNotNull(element);
        assertEquals(element.attr("class"), "btn btn-success");
    }

    @Test
    public void findSimilarElementCssSelector() throws SimilarElementNotFoundException {
        assertEquals("#page-wrapper > div.row:nth-child(3) > div.col-lg-8 " +
                        "> div.panel.panel-default > div.panel-body > a.btn.btn-success",
                elementFinder.findSimilarElementCssSelector(original, another));
    }
}